package com.example.exampleliqubase.dao;

import java.util.List;
import java.util.Optional;

import com.example.exampleliqubase.model.Account;
import org.apache.ibatis.annotations.Select;

public interface AccountMapper {
    int deleteByPrimaryKey(Long userId);

    int insert(Account record);

    int insertSelective(Account record);

    Account selectByPrimaryKey(Long userId);

    int updateByPrimaryKeySelective(Account record);

    int updateByPrimaryKey(Account record);

    Optional<Account> getByLogin(String login);

    @Select("select r.name from account a " +
            "inner join account_roles ar on a.user_id = ar.account_id " +
            "inner join roles r on r.roles_id = ar.role_id " +
            "where a.user_id=#{userId}")
    List<String> getAuthoritiesById(Long userId);
}