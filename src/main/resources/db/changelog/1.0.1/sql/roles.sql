create sequence if not exists roles_sq start 1;

create table roles
(
    roles_id bigint default nextval('person_seq'::regclass) not null
        constraint role_pk
            primary key,
    name     varchar(100)
);

create table account_roles
(
    account_id bigint,
    role_id   bigint,
    primary key (account_id, role_id),
    constraint acc_fk
        foreign key (account_id)
            references account(user_id),
    constraint rol_fk
        foreign key (role_id)
            references roles(roles_id)
);