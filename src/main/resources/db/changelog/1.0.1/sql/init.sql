insert into roles (roles_id, name)
values (1, 'USER'),
       (2, 'ADMIN');

insert into account(user_id, username, password, country)
values (1, 'Admin', '$2y$12$.ZKYHnJrgVNgJixQvjqjBOysdZdEb2HuLz.LCIIB01GuNUExVljtG', 'R');

insert into account_roles(account_id, role_id)
values (1, 1),
       (1, 2);